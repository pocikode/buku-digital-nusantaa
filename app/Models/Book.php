<?php

namespace App\Models;

use App\Traits\GenerateSlug;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Umutphp\LaravelModelRecommendation\HasRecommendation;
use Umutphp\LaravelModelRecommendation\InteractWithRecommendation;

class Book extends Model implements InteractWithRecommendation
{
    use HasFactory, GenerateSlug, HasRecommendation;

    protected $guarded = [];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function imageUrl(): Attribute
    {
        return Attribute::get(function () {
            if (Str::startsWith($this->image, 'http')) {
                return $this->image;
            }

            return Storage::url($this->image);
        });
    }

    public static function getRecommendationConfig(): array
    {
        return [
            'similar_books' => [
                'recommendation_algorithm' => 'similarity',
                'similarity_feature_weight' => 3,
                'similarity_numeric_value_weight' => 0.5,
                'similarity_numeric_value_high_range' => 20,
                'similarity_taxonomy_weight' => 2,
                'similarity_feature_attributes' => [
                    'author'
                ],
                'similarity_numeric_value_attributes' => [
                    'price'
                ],
                'similarity_taxonomy_attributes' => [
                    [
                        'category' => 'name',
                    ]
                ],
                'recommendation_count' => 5,
                'recommendation_order' => 'desc',
            ],
            'sold_together' => [
                'recommendation_algorithm'         => 'db_relation',
                'recommendation_data_table'        => 'order_details',
                'recommendation_data_table_filter' => [],
                'recommendation_data_field'        => 'book_id',
                'recommendation_data_field_type'   => self::class,
                'recommendation_group_field'       => 'order_id',
                'recommendation_count'             => 5
            ],
        ];
    }
}
