<?php

namespace App\Console\Commands;

use App\Models\Book;
use Illuminate\Console\Command;

class GenerateRecommendation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-recommendation {name : The name of recommendation config}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Book Recommendation';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        match ($this->argument('name')) {
            'similar_books' => Book::generateRecommendations('similar_books'),
            'sold_together' => Book::generateRecommendations('sold_together'),
        };
    }
}
