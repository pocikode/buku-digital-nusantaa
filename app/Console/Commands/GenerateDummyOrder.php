<?php

namespace App\Console\Commands;

use App\Models\Book;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\User;
use App\Models\UserBook;
use Illuminate\Console\Command;

class GenerateDummyOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-dummy-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $books = Book::all();
        $users = User::all();

        for ($i = 0; $i < 1000; $i++) {
            $user = $users->random();
            $orderedBooks = $books->random(rand(2, 5));

            $order = Order::create([
                'invoice' => 'INV-' . str_pad(rand(1, 999999), 6, '0', STR_PAD_LEFT),
                'user_id' => $user->id,
                'unique_code' => rand(100000, 999999),
                'sub_total' => rand(50000, 200000),  // Asumsi harga subtotal
                'total_price' => rand(60000, 250000), // Harga total (dengan diskon/biaya tambahan)
                'status' => 'SUCCESS', // Order selesai
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            // Masukkan detail buku dalam order
            foreach ($orderedBooks as $book) {
                OrderDetail::create([
                    'order_id' => $order->id,
                    'book_id' => $book->id,
                    'detail' => 'Purchased ' . $book->title,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }

            // Insert user_books untuk mencatat buku yang telah dibeli oleh pengguna
            foreach ($orderedBooks as $book) {
                UserBook::create([
                    'user_id' => $user->id,
                    'book_id' => $book->id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
    }
}
