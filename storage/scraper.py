import requests
from bs4 import BeautifulSoup
import json
import os

PAGE_URL = "https://books.toscrape.com/catalogue/page-{}.html"
BOOK_URL = 'https://books.toscrape.com/catalogue/{}'
BASE_URL = "https://books.toscrape.com/"

def scrape_page(page_number):
    url = PAGE_URL.format(page_number)
    response = requests.get(url)

    if response.status_code != 200:
        print(f'Failed to retrieve page {page_number}')
        return []

    soup = BeautifulSoup(response.content, 'html.parser')
    books = soup.find_all('article', class_='product_pod')

    book_data = []

    for book in books:
        # Book title
        title = book.h3.a['title']

        # Price
        price = book.find('p', class_='price_color').text

        # Book detail URL (to retrieve category and description)
        book_detail_url = book.find('h3').find('a')['href']
        book_detail_url = BOOK_URL.format(book_detail_url)

         # Get book detail for category and description
        category, image_url, description = scrape_book_details(book_detail_url)

        # Append data
        book_data.append({
            'title': title,
            'price': price,
            'image_url': image_url,
            'category': category,
            'description': description
        })

    return book_data

def scrape_book_details(book_url):
    response = requests.get(book_url)
    soup = BeautifulSoup(response.content, 'html.parser')

    # Category (find breadcrumb and extract category)
    category = 'Others'
    breadcrumb = soup.find('ul', class_='breadcrumb')
    if breadcrumb:
        category_list = breadcrumb.find_all('li')
        if len(category_list) > 2:
            category = category_list[2].text.strip()

    image_element = soup.find('div', id='product_gallery').find('img')
    image_url = ''
    if image_element:
        image_url = image_element['src'].replace('../../', BASE_URL)

    # Description (find the product description if available)
    description_element = soup.find('meta', {'name': 'description'})
    description = description_element['content'].strip() if description_element else 'No description available'

    return category, image_url, description

def save_to_json(data, filename):
    if os.path.exists(filename):
        # If the file exists, load existing data and append new data
        with open(filename, 'r', encoding='utf-8') as f:
            existing_data = json.load(f)
        existing_data.extend(data)
        with open(filename, 'w', encoding='utf-8') as f:
            json.dump(existing_data, f, ensure_ascii=False, indent=4)
    else:
        # If the file does not exist, create a new file and save data
        with open(filename, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

def scrape_books(pages):
    for page in range(1, pages + 1):
        print(f"Scraping page {page}")
        books = scrape_page(page)
        if books:
            save_to_json(books, 'books.json')

        print(f"Page {page} scraped and saved.")


if __name__ == '__main__':
    scrape_books(50)
    print("Scraping completed and data saved to books.json")
