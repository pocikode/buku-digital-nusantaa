<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
//        $sd = Category::query()->where('name', 'SD')->first();
//        $smp = Category::query()->where('name', 'SMP')->first();
//        $sma = Category::query()->where('name', 'SMA')->first();
//        Book::factory(7)->for($sd)->create();
//        Book::factory(7)->for($smp)->create();
//        Book::factory(7)->for($sma)->create();

        $dummyImage = fake()->image(null, 350, 500);
        $dummyBookFile = storage_path('dummy.pdf');
        if (!file_exists($dummyBookFile)) {
            file_put_contents($dummyBookFile, 'This is a dummy PDF file for testing purposes.');
        }

        $bookPath = Storage::putFile('book-file', new File($dummyBookFile));

        $books_data = json_decode(file_get_contents(storage_path('books.json')), false);
        foreach ($books_data as $bookData) {
            $category = Category::firstOrCreate([
                'name' => $bookData->category,
                'slug' => Str::slug($bookData->category),
            ]);

            $imageFile = storage_path('seeder_images/'. $bookData->image_file);
            $imagePath = Storage::putFile('public/books-image', new File(
                file_exists($imageFile) ? $imageFile : $dummyImage
            ));

            $book = new Book();
            $book->category_id = $category->id;
            $book->image = $imagePath;
            $book->title = $bookData->title;
            $book->slug = Str::slug($bookData->title);
            $book->description = $bookData->description;
            $book->author = $bookData->author;
            $book->price = $bookData->price === 0 ? 5000 : $bookData->price;
            $book->file_path = $bookPath;
            $book->save();
        }
    }
}
